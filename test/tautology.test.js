import { firstBook, computePriceOfShoppingBasket, secondBook, thirdBook, fourthBook, fifthBook } from '../src/Books';

describe('Potter kata', () => {
  it('costs 8 euros to buy one Potter book', () => {
    expect(computePriceOfShoppingBasket([firstBook])).toEqual(8);
  });
  it('costs 15.2 euros to buy two different Potter book', () => {
    expect(computePriceOfShoppingBasket([firstBook, secondBook])).toEqual(15.2);
  });
  it('costs 21.6 euros to buy three different Potter book', () => {
    expect(computePriceOfShoppingBasket([firstBook, secondBook, thirdBook])).toEqual(21.6);
  });
  it('costs 27.2 euros to buy four different Potter book', () => {
    expect(computePriceOfShoppingBasket([
      firstBook, secondBook, thirdBook, fourthBook,
    ])).toEqual(27.2);
  });
  it('costs 31.5 euros to buy five different Potter book', () => {
    expect(computePriceOfShoppingBasket([
      firstBook, secondBook, thirdBook, fourthBook, fifthBook,
    ])).toEqual(31.5);
  });

  it('costs 16 euros to buy twice the first book', () => {
    expect(computePriceOfShoppingBasket([
      firstBook, firstBook,
    ])).toEqual(16);
  });
  it('costs 51.20 to buy the example', () => {
    expect(computePriceOfShoppingBasket([
      firstBook, firstBook, secondBook, secondBook, thirdBook, thirdBook, fourthBook, fifthBook,
    ])).toEqual(51.20);
  });
});
