/* eslint-disable no-restricted-syntax */
export const firstBook = 'first book';
export const secondBook = 'second book';
export const thirdBook = 'third book';
export const fourthBook = 'fourth book';
export const fifthBook = 'fifth book';

const priceOfBookSet = {
  1: 8,
  2: 15.2,
  3: 21.6,
  4: 27.2,
  5: 31.5,
};

export function computePriceOfShoppingBasket(shoppingBasket) {
  const countingBookMap = shoppingBasket.reduce((acc, book) => {
    if (acc[book] !== undefined) {
      acc[book] += 1;
    } else {
      acc[book] = 1;
    }
    return acc;
  }, {});
  let booksAmounts = Object.values(countingBookMap); // [5, 3, 4]
  let price = 0;
  while (booksAmounts.length > 0) {
    price += priceOfBookSet[booksAmounts.length];
    booksAmounts = booksAmounts.map((amount) => amount - 1).filter((value) => value > 0);
  }
  return price; // priceOfBookSet[shoppingBasket.length];
}
